rootProject.name = "_-12-03459867tm-ao-ae-wcw11111111-___-_43"
include("src:main:ui")

buildscript {
    repositories {
        gradlePluginPortal()
        maven(url = "https://dl.equo.dev/gradle/0.6.2")
    }

    dependencies {
        classpath("com.equo:com.equo.gradle.plugin:0.6.2")
    }
}

apply(plugin = "com.equo")
